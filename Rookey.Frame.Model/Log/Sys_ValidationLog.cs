﻿using Rookey.Frame.EntityBase.Attr;
using Rookey.Frame.Model.EnumSpace;
using ServiceStack.DataAnnotations;
using System;

namespace Rookey.Frame.Model.Log
{
    /// <summary>
    /// 验证码日志
    /// </summary>
    [NoModule]
    public class Sys_ValidationLog : BaseLogEntity
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [NoField]
        public string UserName { get; set; }

        /// <summary>
        /// 验证方式
        /// </summary>
        [NoField]
        public string VerifyMethod { get; set; }

        /// <summary>
        /// 验证账号
        /// </summary>
        [NoField]
        public string VerifyAccount { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        [NoField]
        public string VerifyCode { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        [NoField]
        public string IPAddress { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [NoField]
        public string Description { get; set; }

        /// <summary>
        /// 验证状态
        /// </summary>
        [NoField]
        public string Status { get; set; }

        /// <summary>
        /// MD5
        /// </summary>
        [NoField]
        public string MD5 { get; set; }
    }
}
