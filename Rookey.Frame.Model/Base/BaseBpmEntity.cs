﻿/*----------------------------------------------------------------
        // Copyright (C) Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // 
//----------------------------------------------------------------*/

using Rookey.Frame.EntityBase;

namespace Rookey.Frame.Model
{
    /// <summary>
    /// 工作流实体基类
    /// </summary>
    public class BaseBpmEntity : BaseEntity
    {
    }
}
