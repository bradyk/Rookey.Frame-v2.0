﻿
//重写主键格式化
function OverTitleKeyFormatter(value, row, index, moduleName, fieldName, paramsObj, otherParams) {
    var v = value != undefined && value != null ? value : "";
    if (fieldName == "Code" || fieldName == "Title") {
        return "<a href='javascript:void(0);' title='" + v + "'  todoId='" + row.Id + "' moduleId='" + row.ModuleId + "' recordId='" + row.RecordId + "' t_title='" + encodeURI(row.Title) + "' onclick='ViewRecord(this)'>" + value + "</a>";
    }
    return v;
}

//重写通用格式化
function OverGeneralFormatter(value, row, index, moduleName, fieldName, paramsObj) {
    var v = value != undefined && value != null ? value : "";
    if (fieldName == "Code" || fieldName == "Title") {
        return OverTitleKeyFormatter(value, row, index, moduleName, fieldName, paramsObj);
    }
    return v;
}

//重写枚举字段格式化
function OverEnumFieldFormatter(value, row, index, moduleName, fieldName, enumData, paramsObj) {
    var v = value != undefined && value != null ? value : "";
    if (v && enumData && enumData.length > 0) {
        var dic = eval("(" + decodeURIComponent(enumData) + ")");
        if (dic && dic.length > 0) {
            for (var i = 0; i < dic.length; i++) {
                var tempV = dic[i].Id;
                var n = dic[i].Name;
                if (value == tempV && tempV != "") {
                    v = n;
                    break;
                }
            }
        }
    }
    if (fieldName == "Status") {
        return "<font color='red'>" + v + "</font>";
    }
    return v;
}

//重写外键格式化
function OverForeignKeyFormatter(value, row, index, moduleName, fieldName, foreignModuleName, paramsObj, otherForeignParams) {
    var v = value != undefined && value != null ? value : "";
    if (fieldName == "OrgM_EmpName") {
        return "<span>" + v + "</span>";
    }
    return v;
}

//查看记录
function ViewRecord(obj) {
    var todoId = $(obj).attr('todoId');
    var moduleId = $(obj).attr('moduleId');
    var recordId = $(obj).attr('recordId');
    var title = $(obj).attr('t_title');
    var tp = GetLocalQueryString("p_tp");
    var url = "/Page/EditForm.html?page=edit&mode=1&moduleId=" + moduleId + "&id=" + recordId + "&todoId=" + todoId;
    if (tp != 0)
        url = "/Page/ViewForm.html?page=view&mode=1&moduleId=" + moduleId + "&id=" + recordId;
    if (tp == 0) {
        var currTabIndex = GetSelectTabIndex(); //当前grid网格页面的tabindex
        if (currTabIndex)
            url += "&tb=" + currTabIndex;
    }
    url += "&r=" + Math.random();
    AddTab(null, decodeURI(title), url);
}