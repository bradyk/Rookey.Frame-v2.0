﻿using Rookey.Frame.Base;
using Rookey.Frame.Model.OrgM;
using Rookey.Frame.Model.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.Operate.Base.OperateHandle.Implement
{
    /// <summary>
    /// 用户角色操作事件处理
    /// </summary>
    public class Sys_UserRoleOperateHandle : IModelOperateHandle<Sys_UserRole>
    {
        /// <summary>
        /// 用户角色操作完成事件
        /// </summary>
        /// <param name="operateType">操作类型</param>
        /// <param name="t">对象</param>
        /// <param name="result">结果</param>
        /// <param name="currUser">当前用户</param>
        /// <param name="otherParams"></param>
        public void OperateCompeletedHandle(ModelRecordOperateType operateType, Sys_UserRole t, bool result, UserInfo currUser, object[] otherParams = null)
        {
            if (result && (operateType == ModelRecordOperateType.Add || operateType == ModelRecordOperateType.Edit || 
                operateType == ModelRecordOperateType.Del || operateType == ModelRecordOperateType.Restore))
            {
                if(t.Sys_UserId.HasValue && t.Sys_UserId.Value!=Guid.Empty)
                {
                //用户角色新增、删除、修改、还原时将当前用户对应的上级用户角色缓存数据刷新
                   OrgM_Emp emp = OrgMOperate.GetEmpByUserId(t.Sys_UserId.Value);
                   if (emp != null)
                   {
                       List<OrgM_Dept> depts = OrgMOperate.GetEmpDepts(emp.Id);
                       if (depts != null && depts.Count > 0)
                       {
                           foreach (OrgM_Dept dept in depts)
                           {
                              OrgM_Emp leader = OrgMOperate.GetDeptLeader(dept.Id);
                              if (leader != null)
                              {
                                 Guid userId = OrgMOperate.GetUserIdByEmpId(leader.Id);
                                 PermissionOperate.ClearChildUserRolesCaches(userId);
                              }
                           }
                       }
                   }
                }
            }
        }

        /// <summary>
        /// 操作前
        /// </summary>
        /// <param name="operateType">操作类型</param>
        /// <param name="t">对象</param>
        /// <param name="errMsg">异常信息</param>
        /// <param name="otherParams"></param>
        /// <returns></returns>
        public bool BeforeOperateVerifyOrHandle(ModelRecordOperateType operateType, Sys_UserRole t, out string errMsg, object[] otherParams = null)
        {
            errMsg = string.Empty;
            return true;
        }

        /// <summary>
        /// 用户角色操作完成事件
        /// </summary>
        /// <param name="operateType">操作类型</param>
        /// <param name="ts">对象集合</param>
        /// <param name="result">结果</param>
        /// <param name="currUser">当前用户</param>
        /// <param name="otherParams"></param>
        public void OperateCompeletedHandles(ModelRecordOperateType operateType, List<Sys_UserRole> ts, bool result, UserInfo currUser, object[] otherParams = null)
        {
            if (ts != null && ts.Count > 0)
            {
                foreach (Sys_UserRole t in ts)
                {
                    OperateCompeletedHandle(operateType, t, result, currUser, otherParams);
                }
            }
        }

        public bool BeforeOperateVerifyOrHandles(ModelRecordOperateType operateType, List<Sys_UserRole> ts, out string errMsg, object[] otherParams = null)
        {
            errMsg = string.Empty;
            return true;
        }
    }
}
