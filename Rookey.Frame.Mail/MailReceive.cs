﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.Mail
{
    /// <summary>
    /// 邮件接收
    /// </summary>
    public class MailReceive
    {
        private Pop3Helper _pop3Helper = null; //邮件对象
        private MailReceiveParams _mailReceiveParams = null; //邮件发送参数
        private bool _isAsync = true; //是否异步发送邮件

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="receiveParams">接收参数</param>
        /// <param name="isAsync">是否异步</param>
        public MailReceive(MailReceiveParams receiveParams, bool isAsync = true)
        {
            if (receiveParams == null) throw new Exception("邮件接收参数不能为空！");
            this._mailReceiveParams = receiveParams;
            this._isAsync = isAsync;
            this._pop3Helper = new Pop3Helper(receiveParams.EmailType, receiveParams.Username, receiveParams.Password, receiveParams.EnabledSsl);
            this._pop3Helper.EmailReceiveEvent += new EventHandler<Pop3EventArgs>(ReceveMessageHanlder);
        }

        /// <summary>
        /// 接收邮件
        /// </summary>
        public void Receive()
        {
            this._pop3Helper.ReceiveMails();
        }

        /// <summary>
        /// 接收邮件事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReceveMessageHanlder(object sender, Pop3EventArgs e)
        {
            if (e.Message != null)
            {

            }
        }
    }
}
